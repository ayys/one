# one
A translator for GNU/Hurd that outputs a continuous stream of `1'

##setup

Install the hurd-one.c file using the command :

```
  gcc -o one hurd-one.c -lports -ltrivfs
```

Then set the server to a node(a file, since it is a trivfs translator)
using the following command

```
  settrans -c none <absolute path to the `one' file as compiled above>
```
Note : The -c flag will create the file `none' if it does not exist

##Usage
After setup, reading from the none file will output an infinite
(cancelled by ctrl-c ofcourse) stream of 1s.

Try 
```cat none```
