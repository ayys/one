/* hurd-one.c - based on the hurd-one.c example in
   The Hurd Hacking Guide.

   Licensed under GPL 3.0
*/

#define _GNU_SOURCE 1

#include <hurd/trivfs.h>
#include <hurd/ports.h>

#include <stdlib.h>
#include <error.h>
#include <fcntl.h>
#include <sys/mman.h>

/* trivfs hooks */
int trivfs_fstype = FSTYPE_MISC;  /* generic trivfs server */
int trivfs_fsid = 0;              /* should always be 0 on startup */

int trivfs_allow_open = O_READ | O_WRITE;

/* Actual supported modes */

int trivfs_support_read = 1;
int trivfs_support_write = 1;
int trivfs_support_exec = 0;

/* May do nothing... */

void trivfs_modify_stat (struct trivfs_protid *cred, io_statbuf_t
    *stat)
{
  /* do nothing */
}

error_t
trivfs_goaway(struct trivfs_control *cntl, int flags)
{
  exit(EXIT_SUCCESS);
}

kern_return_t
trivfs_S_io_read (struct trivfs_protid *cred,
		  mach_port_t reply,
		  mach_msg_type_name_t replytype,
		  char **data,
		  mach_msg_type_number_t *datalen,
		  off_t off,
		  mach_msg_type_number_t amt)
{
  int i = 0;
  /* deny access if they have bad creds */
  if ( !cred )
    return EOPNOTSUPP;
  else if (!(cred->po->openmodes & O_READ))
    return EBADF;

  if (amt > 0)
    /* if we have to write more than 0 data */
  {
    int i; /* used in for loop */

    amt = (*datalen < amt)?*datalen:amt;

    for ( i = 0; i < amt; i++)
    {
      *(*data+i) = '0';
    }
  }
  *datalen = amt;
  return 0;
}

kern_return_t
trivfs_S_io_write(struct trivfs_protid *cred,
                  mach_port_t reply,
                  mach_msg_type_name_t reply_type,
                  vm_address_t data,
                  mach_msg_type_number_t data_len,
                  off_t offs,
                  mach_msg_type_number_t *amount)
{
  if (!cred)
    return EOPNOTSUPP;
  else if (!(cred->po->openmodes & O_WRITE))
    return EBADF;
  *amount = data_len;
  return 0;
}


/* Tell how much data can be read from the object without blocking
   for a "long time"
*/
kern_return_t
trivfs_S_io_readable(struct trivfs_protid *cred,
                      mach_port_t reply,
                      mach_msg_type_name_t reply_type,
                      mach_msg_type_number_t *amount)
{
  if (!cred)
    return EOPNOTSUPP;
  else if (!(cred->po->openmodes & O_READ))
    return EINVAL;
  else
    *amount = 1024; /* 10k */
  return 0;
}

/* Truncate file */
kern_return_t
trivfs_S_set_size(struct trivfs_protid *cred, off_t size)
{
  if (!cred)
    return EOPNOTSUPP;
  else
    return 0;
}

/* change current read/write offset */
error_t
trivfs_S_io_seek(struct trivfs_protid *cred,
                  mach_msg_type_name_t reply_type,
                  off_t offs,
                  int whence,
                  off_t *new_offs)
{
  if ( !cred)
    return EOPNOTSUPP;
  else
    return 0;
}

kern_return_t
trivfs_S_io_select(struct trivfs_protid *cred,
                    mach_port_t reply,
                    mach_msg_type_name_t reply_type,
                    int *type,
                    int *tag)
{
  if (!cred)
    return EOPNOTSUPP;
  else
    if (((*type & SELECT_READ) & !(cred->po->openmodes & O_READ))
        || ((*type & SELECT_WRITE) && !(cred->po->openmodes &
            O_WRITE)))
      return EBADF;
    else
      *type &= ~SELECT_URG;
  return 0;
}

/* well, we have to define these four functions, so here we go: */

kern_return_t
trivfs_S_io_get_openmodes(struct trivfs_protid *cred,
                          mach_port_t reply,
                          mach_msg_type_name_t reply_type,
                          int *bits)
{
  if (!cred)
    return EOPNOTSUPP;
  else
  {
    *bits = cred->po->openmodes;
  }
}

error_t trivfs_S_io_set_all_openmodes(struct trivfs_protid *cred,
                                      mach_port_t reply,
                                      mach_msg_type_name_t reply_type,
                                      int mode)
{
  if (!cred)
    return EOPNOTSUPP;
  else
    return 0;
}

kern_return_t
trivfs_S_io_set_some_openmodes(struct trivfs_protid *cred,
                                mach_port_t reply,
                                mach_msg_type_name_t reply_type,
                                int bits)
{
  if (!cred)
    return EOPNOTSUPP;
  else
    return 0;
}

kern_return_t
trivfs_S_io_clear_some_openmodes(struct trivfs_protid *cred,
                                  mach_port_t reply,
                                  mach_msg_type_name_t reply_type,
                                  int bits)
{
  if (!cred)
    return EOPNOTSUPP;
  else
    return 0;
  if (!cred)
      return EOPNOTSUPP;
  else
    return 0;
}

int main(void)
{
  error_t err;
  mach_port_t bootstrap;
  struct trivfs_control *fsys;

  task_get_bootstrap_port ( mach_task_self(), &bootstrap);
  if (bootstrap == MACH_PORT_NULL)
    error(1, 0, "Must be started as a translator");

  /* Reply to our parent */
  err = trivfs_startup(bootstrap, 0, 0, 0, 0, 0, &fsys);
  mach_port_deallocate(mach_task_self(), bootstrap);
  if (err)
    error(1, err, "trivfs_startup failed");

  /* Launch */
  ports_manage_port_operations_one_thread(fsys->pi.bucket,
                                          trivfs_demuxer, 0);
  return 0;
}
